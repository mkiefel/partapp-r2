part {
  part_id: 1

  part_pos: 0
  part_pos: 1

  part_x_axis_from: 1
  part_x_axis_to: 0
  part_x_axis_offset: -90

  ext_x_pos: 15
  ext_x_neg: 15
  ext_y_pos: 0
  ext_y_neg: 0
}

part {
  part_id: 2

  part_pos: 1
  part_pos: 2

  part_x_axis_from: 2
  part_x_axis_to: 1
  part_x_axis_offset: -90

  ext_x_pos: 15
  ext_x_neg: 15
  ext_y_pos: 0
  ext_y_neg: 0
}


part {
  part_id: 3

  part_pos: 4
  part_pos: 3

  part_x_axis_from: 3
  part_x_axis_to: 4
  part_x_axis_offset: -90

  ext_x_pos: 15
  ext_x_neg: 15
  ext_y_pos: 0
  ext_y_neg: 0
}

part {
  part_id: 4

  part_pos: 5
  part_pos: 4

  part_x_axis_from: 4
  part_x_axis_to: 5
  part_x_axis_offset: -90

  ext_x_pos: 15
  ext_x_neg: 15
  ext_y_pos: 0
  ext_y_neg: 0
}

part {
  part_id: 5

  part_pos: 6
  part_pos: 7

  part_x_axis_from: 7
  part_x_axis_to: 6
  part_x_axis_offset: -90

  ext_x_pos: 15
  ext_x_neg: 15
  ext_y_pos: 0
  ext_y_neg: 0
}

part {
  part_id: 6

  part_pos: 7
  part_pos: 8

  part_x_axis_from: 8
  part_x_axis_to: 7
  part_x_axis_offset: -90

  ext_x_pos: 15
  ext_x_neg: 15
  ext_y_pos: 0
  ext_y_neg: 0
}


part {
  part_id: 7

  part_pos: 9
  part_pos: 10

  part_x_axis_from: 9
  part_x_axis_to: 10
  part_x_axis_offset: -90

  ext_x_pos: 15
  ext_x_neg: 15
  ext_y_pos: 0
  ext_y_neg: 0
}


part {
  part_id: 8

  part_pos: 10
  part_pos: 11

  part_x_axis_from: 10
  part_x_axis_to: 11
  part_x_axis_offset: -90

  ext_x_pos: 15
  ext_x_neg: 15
  ext_y_pos: 0
  ext_y_neg: 0
}

part {
  part_id: 9

  part_pos: 12
  part_pos: 13

  part_x_axis_from: 13
  part_x_axis_to: 12
  part_x_axis_offset: -90

  ext_x_pos: 20
  ext_x_neg: 20
  ext_y_pos: 10
  ext_y_neg: 10
}

part {
  part_id: 10

  part_pos: 14
  part_pos: 15

  part_x_axis_from: 15
  part_x_axis_to: 14
  part_x_axis_offset: -90

  ext_x_pos: 30
  ext_x_neg: 30
  ext_y_pos: 10
  ext_y_neg: 10

  is_root: true
}

joint {
  child_idx: 1
  parent_idx: 2

  type: "RotGaussian"

  joint_pos: 1
}

joint {
  child_idx: 2
  parent_idx: 10

  type: "RotGaussian"

  joint_pos: 2
}

joint {
  child_idx: 4
  parent_idx: 3

  type: "RotGaussian"

  joint_pos: 4
}

joint {
  child_idx: 3
  parent_idx: 10

  type: "RotGaussian"

  joint_pos: 3
}

joint {
  child_idx: 5
  parent_idx: 6

  type: "RotGaussian"

  joint_pos: 7
}

joint {
  child_idx: 6
  parent_idx: 10

  type: "RotGaussian"

  joint_pos: 8
}

joint {
  child_idx: 8
  parent_idx: 7

  type: "RotGaussian"

  joint_pos: 10
}

joint {
  child_idx: 7
  parent_idx: 10

  type: "RotGaussian"

  joint_pos: 9
}

joint {
  child_idx: 9
  parent_idx: 10

  type: "RotGaussian"

  joint_pos: 12
  joint_pos: 15
}

app_group {
  part_id: 1
  part_id: 4	   
}	  

app_group {
  part_id: 2
  part_id: 3	   
}	  

app_group {
  part_id: 5
  part_id: 8
}	  

app_group {
  part_id: 6
  part_id: 7	   
}	  

app_group {
  part_id: 9
}	  

app_group {
  part_id: 10 	  
}