// Generated by the protocol buffer compiler.  DO NOT EDIT!

#include "PartConfig.pb.h"
#include <google/protobuf/descriptor.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format_inl.h>

namespace {

const ::google::protobuf::Descriptor* PartDef_descriptor_ = NULL;
const ::google::protobuf::internal::GeneratedMessageReflection*
  PartDef_reflection_ = NULL;
const ::google::protobuf::Descriptor* Joint_descriptor_ = NULL;
const ::google::protobuf::internal::GeneratedMessageReflection*
  Joint_reflection_ = NULL;
const ::google::protobuf::Descriptor* AppearanceGroup_descriptor_ = NULL;
const ::google::protobuf::internal::GeneratedMessageReflection*
  AppearanceGroup_reflection_ = NULL;
const ::google::protobuf::Descriptor* PartConfig_descriptor_ = NULL;
const ::google::protobuf::internal::GeneratedMessageReflection*
  PartConfig_reflection_ = NULL;

}  // namespace


void protobuf_BuildDesc_PartConfig_2eproto() {
  static bool already_here = false;
  if (already_here) return;
  already_here = true;
  GOOGLE_PROTOBUF_VERIFY_VERSION;
  ::google::protobuf::DescriptorPool* pool =
    ::google::protobuf::DescriptorPool::internal_generated_pool();

  const ::google::protobuf::FileDescriptor* file = pool->InternalBuildGeneratedFile(
    "\n\020PartConfig.proto\"\241\002\n\007PartDef\022\017\n\007part_i"
    "d\030\001 \001(\005\022\020\n\010part_pos\030\002 \003(\005\022\030\n\020part_x_axis"
    "_from\030\004 \001(\005\022\026\n\016part_x_axis_to\030\005 \001(\005\022\035\n\022p"
    "art_x_axis_offset\030\006 \001(\002:\0010\022\024\n\text_x_pos\030"
    "\007 \001(\002:\0010\022\024\n\text_x_neg\030\010 \001(\002:\0010\022\024\n\text_y_"
    "pos\030\t \001(\002:\0010\022\024\n\text_y_neg\030\n \001(\002:\0010\022\026\n\007is"
    "_root\030\013 \001(\010:\005false\022\027\n\tis_detect\030\014 \001(\010:\004t"
    "rue\022\031\n\nis_upright\030\r \001(\010:\005false\"Y\n\005Joint\022"
    "\021\n\tchild_idx\030\001 \001(\005\022\022\n\nparent_idx\030\002 \001(\005\022\026"
    "\n\004type\030\003 \001(\t:\010Gaussian\022\021\n\tjoint_pos\030\004 \003("
    "\005\"\"\n\017AppearanceGroup\022\017\n\007part_id\030\001 \003(\005\"`\n"
    "\nPartConfig\022\026\n\004part\030\001 \003(\0132\010.PartDef\022\025\n\005j"
    "oint\030\002 \003(\0132\006.Joint\022#\n\tapp_group\030\003 \003(\0132\020."
    "AppearanceGroup", 535);
  PartDef_descriptor_ = file->message_type(0);
  PartDef_reflection_ =
    new ::google::protobuf::internal::GeneratedMessageReflection(
      PartDef_descriptor_,
      &PartDef::default_instance(),
      PartDef::_offsets_,
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PartDef, _has_bits_[0]),
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PartDef, _unknown_fields_),
      -1,
      ::google::protobuf::DescriptorPool::generated_pool());
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedMessage(
    PartDef_descriptor_, &PartDef::default_instance());
  Joint_descriptor_ = file->message_type(1);
  Joint_reflection_ =
    new ::google::protobuf::internal::GeneratedMessageReflection(
      Joint_descriptor_,
      &Joint::default_instance(),
      Joint::_offsets_,
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(Joint, _has_bits_[0]),
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(Joint, _unknown_fields_),
      -1,
      ::google::protobuf::DescriptorPool::generated_pool());
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedMessage(
    Joint_descriptor_, &Joint::default_instance());
  AppearanceGroup_descriptor_ = file->message_type(2);
  AppearanceGroup_reflection_ =
    new ::google::protobuf::internal::GeneratedMessageReflection(
      AppearanceGroup_descriptor_,
      &AppearanceGroup::default_instance(),
      AppearanceGroup::_offsets_,
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(AppearanceGroup, _has_bits_[0]),
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(AppearanceGroup, _unknown_fields_),
      -1,
      ::google::protobuf::DescriptorPool::generated_pool());
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedMessage(
    AppearanceGroup_descriptor_, &AppearanceGroup::default_instance());
  PartConfig_descriptor_ = file->message_type(3);
  PartConfig_reflection_ =
    new ::google::protobuf::internal::GeneratedMessageReflection(
      PartConfig_descriptor_,
      &PartConfig::default_instance(),
      PartConfig::_offsets_,
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PartConfig, _has_bits_[0]),
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PartConfig, _unknown_fields_),
      -1,
      ::google::protobuf::DescriptorPool::generated_pool());
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedMessage(
    PartConfig_descriptor_, &PartConfig::default_instance());
}

// Force BuildDescriptors() to be called at static initialization time.
struct StaticDescriptorInitializer_PartConfig_2eproto {
  StaticDescriptorInitializer_PartConfig_2eproto() {
    protobuf_BuildDesc_PartConfig_2eproto();
  }
} static_descriptor_initializer_PartConfig_2eproto_;


// ===================================================================

const PartDef PartDef::default_instance_;













const int PartDef::_offsets_[12] = {
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PartDef, part_id_),
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PartDef, part_pos_),
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PartDef, part_x_axis_from_),
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PartDef, part_x_axis_to_),
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PartDef, part_x_axis_offset_),
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PartDef, ext_x_pos_),
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PartDef, ext_x_neg_),
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PartDef, ext_y_pos_),
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PartDef, ext_y_neg_),
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PartDef, is_root_),
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PartDef, is_detect_),
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PartDef, is_upright_),
};

PartDef::PartDef()
  : _cached_size_(0),
    part_id_(0),
    part_x_axis_from_(0),
    part_x_axis_to_(0),
    part_x_axis_offset_(0),
    ext_x_pos_(0),
    ext_x_neg_(0),
    ext_y_pos_(0),
    ext_y_neg_(0),
    is_root_(false),
    is_detect_(true),
    is_upright_(false) {
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  if (this == &default_instance_) {
  }
}

PartDef::PartDef(const PartDef& from)
  : _cached_size_(0),
    part_id_(0),
    part_x_axis_from_(0),
    part_x_axis_to_(0),
    part_x_axis_offset_(0),
    ext_x_pos_(0),
    ext_x_neg_(0),
    ext_y_pos_(0),
    ext_y_neg_(0),
    is_root_(false),
    is_detect_(true),
    is_upright_(false) {
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  MergeFrom(from);
}

PartDef::~PartDef() {
  if (this != &default_instance_) {
  }
}

const ::google::protobuf::Descriptor* PartDef::descriptor() {
  if (PartDef_descriptor_ == NULL) protobuf_BuildDesc_PartConfig_2eproto();
  return PartDef_descriptor_;
}

PartDef* PartDef::New() const {
  return new PartDef;
}

const ::google::protobuf::Descriptor* PartDef::GetDescriptor() const {
  return descriptor();
}

const ::google::protobuf::Reflection* PartDef::GetReflection() const {
  if (PartDef_reflection_ == NULL) protobuf_BuildDesc_PartConfig_2eproto();
  return PartDef_reflection_;
}

// ===================================================================

const Joint Joint::default_instance_;



const ::std::string Joint::_default_type_("Gaussian");

const int Joint::_offsets_[4] = {
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(Joint, child_idx_),
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(Joint, parent_idx_),
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(Joint, type_),
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(Joint, joint_pos_),
};

Joint::Joint()
  : _cached_size_(0),
    child_idx_(0),
    parent_idx_(0),
    type_(const_cast< ::std::string*>(&_default_type_)) {
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  if (this == &default_instance_) {
  }
}

Joint::Joint(const Joint& from)
  : _cached_size_(0),
    child_idx_(0),
    parent_idx_(0),
    type_(const_cast< ::std::string*>(&_default_type_)) {
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  MergeFrom(from);
}

Joint::~Joint() {
  if (type_ != &_default_type_) {
    delete type_;
  }
  if (this != &default_instance_) {
  }
}

const ::google::protobuf::Descriptor* Joint::descriptor() {
  if (Joint_descriptor_ == NULL) protobuf_BuildDesc_PartConfig_2eproto();
  return Joint_descriptor_;
}

Joint* Joint::New() const {
  return new Joint;
}

const ::google::protobuf::Descriptor* Joint::GetDescriptor() const {
  return descriptor();
}

const ::google::protobuf::Reflection* Joint::GetReflection() const {
  if (Joint_reflection_ == NULL) protobuf_BuildDesc_PartConfig_2eproto();
  return Joint_reflection_;
}

// ===================================================================

const AppearanceGroup AppearanceGroup::default_instance_;


const int AppearanceGroup::_offsets_[1] = {
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(AppearanceGroup, part_id_),
};

AppearanceGroup::AppearanceGroup()
  : _cached_size_(0) {
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  if (this == &default_instance_) {
  }
}

AppearanceGroup::AppearanceGroup(const AppearanceGroup& from)
  : _cached_size_(0) {
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  MergeFrom(from);
}

AppearanceGroup::~AppearanceGroup() {
  if (this != &default_instance_) {
  }
}

const ::google::protobuf::Descriptor* AppearanceGroup::descriptor() {
  if (AppearanceGroup_descriptor_ == NULL) protobuf_BuildDesc_PartConfig_2eproto();
  return AppearanceGroup_descriptor_;
}

AppearanceGroup* AppearanceGroup::New() const {
  return new AppearanceGroup;
}

const ::google::protobuf::Descriptor* AppearanceGroup::GetDescriptor() const {
  return descriptor();
}

const ::google::protobuf::Reflection* AppearanceGroup::GetReflection() const {
  if (AppearanceGroup_reflection_ == NULL) protobuf_BuildDesc_PartConfig_2eproto();
  return AppearanceGroup_reflection_;
}

// ===================================================================

const PartConfig PartConfig::default_instance_;




const int PartConfig::_offsets_[3] = {
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PartConfig, part_),
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PartConfig, joint_),
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PartConfig, app_group_),
};

PartConfig::PartConfig()
  : _cached_size_(0) {
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  if (this == &default_instance_) {
  }
}

PartConfig::PartConfig(const PartConfig& from)
  : _cached_size_(0) {
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  MergeFrom(from);
}

PartConfig::~PartConfig() {
  if (this != &default_instance_) {
  }
}

const ::google::protobuf::Descriptor* PartConfig::descriptor() {
  if (PartConfig_descriptor_ == NULL) protobuf_BuildDesc_PartConfig_2eproto();
  return PartConfig_descriptor_;
}

PartConfig* PartConfig::New() const {
  return new PartConfig;
}

const ::google::protobuf::Descriptor* PartConfig::GetDescriptor() const {
  return descriptor();
}

const ::google::protobuf::Reflection* PartConfig::GetReflection() const {
  if (PartConfig_reflection_ == NULL) protobuf_BuildDesc_PartConfig_2eproto();
  return PartConfig_reflection_;
}
