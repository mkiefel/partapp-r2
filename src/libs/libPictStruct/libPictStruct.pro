include ( ../begin.pri )

TARGET = PictStruct

###################################################
# HEADERS/SOURCES
###################################################

PB_HEADERS = HypothesisList.pb.h 
PB_SOURCES = HypothesisList.pb.cc

HEADERS = objectdetect.h 
SOURCES = objectdetect_aux.cpp objectdetect_learnparam.cpp objectdetect_findpos.cpp objectdetect_findrot.cpp 

HEADERS += $$PB_HEADERS
SOURCES += $$PB_SOURCES

###################################################
# INCLUDEPATH
###################################################

# TUDVision
#INCLUDEPATH += /home/andriluk/code/TUDVision/src/include

# protocol buffers
INCLUDEPATH += /home/andriluk/include

# Matlab
include( ../../matlab_include.pri )

include ( ../../protobuf.pri)


