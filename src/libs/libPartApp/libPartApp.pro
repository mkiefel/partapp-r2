include ( ../begin.pri )

TARGET = PartApp

###################################################
# HEADERS/SOURCES
###################################################

PB_HEADERS = ExpParam.pb.h 
PB_SOURCES = ExpParam.pb.cc

HEADERS = partapp.h
SOURCES = partapp.cpp

HEADERS += $$PB_HEADERS
SOURCES += $$PB_SOURCES

###################################################
# INCLUDEPATH
###################################################

# TUDVision
#INCLUDEPATH += /home/andriluk/code/TUDVision/src/include

# protocol buffers
INCLUDEPATH += /home/andriluk/include

# Matlab
#INCLUDEPATH += /ultra04/disc8/MATLAB2007a/LINUX/matlab-2007a-linux/extern/include

include( ../../matlab_include.pri )

include ( ../../protobuf.pri)


